import { connect } from 'react-redux';
import React from "react";

class LoadingComponent extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (this.props.isLoading ?
            <div style={{ textAlign: 'center' }}>
                <img src='https://thumbs.gfycat.com/HeartyKeyBubblefish-size_restricted.gif' alt='loading' height='50px' width='50px' />

            </div> :
            null)
    }
}
const mapStateToProps = (state) => {
    return ({
        isLoading: state.BookReducer.isLoading
    })
};


export default connect(mapStateToProps, null)(LoadingComponent);