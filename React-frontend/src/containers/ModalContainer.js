import React from 'react'
import { connect } from 'react-redux'
import ReactModal from 'react-modal'
import { closeModal } from '../actions/ModalActions'
class ModalContainer extends React.Component {
  constructor(props) {
    super(props)
    console.log(this.props)
    this.state = {
      modalIsOpen: this.props.modalProps.open
    }
    this.closeModal = this.closeModal.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.modalProps.open !== this.props.modalProps.open) {
      this.setState({
        modalIsOpen: nextProps.modalProps.open
      })
    }
  }

  closeModal() {
    this.props.hideModal()
  }

  render() {
    if (!this.props.modalType) {
      return null
    }
    return (
      <div>
        <ReactModal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal"
          ariaHideApp={false}
          style={{ overlay: {zIndex: 10}}}
        >

          <h2 ref={subtitle => this.subtitle = subtitle}>Hello</h2>
          <button onClick={this.closeModal}>close</button>
          <div>I am a modal</div>
          <form>
            <input />
            <button>tab navigation</button>
            <button>stays</button>
            <button>inside</button>
            <button>the modal</button>
          </form>
        </ReactModal>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  console.log(state)
  return ({
    modalProps: state.ModalReducer.modalProps,
    modalType: state.ModalReducer.modalType,

  })
};

const mapDispatchToProps = dispatch => {
  return {
    hideModal: () => {
      dispatch(closeModal())
    }

  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModalContainer)