import { connect } from 'react-redux';
import { deleteBooks } from '../actions/BookActions'
import React from "react";
import { Button } from 'antd';
import 'antd/dist/antd.css';

class DeleteButtonComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndexes: [],
            selectedIds: [],

        };
    }



    onRowDelete = () => {
        var newList = this.props.books.filter(
            b => this.props.selectedIds.indexOf(b.id) === -1
        );
        this.props.onDeleteBooks(this.props.selectedIds, newList);

    };


    render() {


        return (
            <Button type="danger" onClick={this.onRowDelete}>
                Delete
        </Button>
        );
    }
}
const mapStateToProps = (state) => {

    return ({
        books: state.BookReducer.books,
        selectedIndexes: state.BookReducer.selectedIndexes,
        selectedIds: state.BookReducer.selectedIds
    })
};

const mapDispatchToProps = dispatch => {
    return {      
        onDeleteBooks: (listIds, listUpdate) => {
            dispatch(deleteBooks(listIds, listUpdate))
        }
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(DeleteButtonComponent);