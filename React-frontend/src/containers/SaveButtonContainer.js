import { connect } from 'react-redux';
import { saveBooks } from '../actions/BookActions'
import React from "react";
import { Button } from 'antd';
import 'antd/dist/antd.css';

class SaveButtonComponent extends React.Component {
    constructor(props) {
        super(props);

    }

    saveBook = () => {
        console.log(this.props.editedBooks)
        this.props.onSaveBooks(this.props.editedBooks)

    };


    render() {

        return (
            <Button
                type="danger"
                onClick={this.saveBook}
                style={{ marginRight: "5px", marginTop: "10px" }}
            >
                Save
        </Button>
        );
    }
}
const mapStateToProps = (state) => {

    return ({

        editedBooks: state.BookReducer.editedBooks
    })
};

const mapDispatchToProps = dispatch => {
    return {
        onSaveBooks: (list) => {
            dispatch(saveBooks(list))
        },
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(SaveButtonComponent);