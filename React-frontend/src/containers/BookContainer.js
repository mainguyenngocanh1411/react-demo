import { connect } from 'react-redux';
import { getBooks, updateListBooks, selectRow, deselectRow, updateEditedBooks } from '../actions/BookActions'
import { openModal } from '../actions/ModalActions'
import React from "react";
import { findDOMNode } from "react-dom";
import { Button } from 'antd';
import 'antd/dist/antd.css';
import { Editors } from "react-data-grid-addons";
import ReactDataGrid from "react-data-grid";
import DeleteButton from "./DeleteButtonContainer"
import SaveButton from "./SaveButtonContainer"
import ModalContainer from "./ModalContainer"
class BookComponent extends React.Component {
    constructor(props) {
        super(props);
        this.bookTableDom = null;
    }
    componentDidMount() {
        //Get initial list books
        this.props.onGetBooks(1, this.props.limit);
        this.bookTableDom = findDOMNode(this).querySelector(".react-grid-Canvas");
        this.bookTableDom.addEventListener("scroll", this.scrollListener);
    }

    rowGetter = i => {
        return this.props.books[i]
    };

    onRowsSelected = rows => {

        var selectedIndexes = this.props.selectedIndexes.concat(rows.map(r => r.rowIdx))
        var selectedIds = this.props.selectedIds.concat(rows.map(r => r.row.id))
        this.props.onSelectRow(selectedIndexes, selectedIds)

    };

    onRowsDeselected = rows => {
        let rowIndexes = rows.map(r => r.rowIdx);
        var selectedIndexes = this.props.selectedIndexes.filter(
            i => rowIndexes.indexOf(i) === -1
        )
        this.props.onDeselectRow(selectedIndexes)
    };

    onGridRowsUpdated = ({ fromRow, toRow, updated }) => {

        const rows = this.props.books.slice();

        for (let i = fromRow; i <= toRow; i++) {

            rows[i] = { ...rows[i], ...updated };

            this.updateEditedBook(rows[i]);
        }
        console.log(rows)
        this.props.onUpdateListBooks(rows)
    };

    updateEditedBook = book => {
        console.log(this.props.editedBooks)
        let books = this.props.editedBooks.filter(b => b.id !== book.id);
        books.push(book);


        this.props.onUpdateEditedBooks(books)
    };


    scrollListener = () => {

        if (
            this.bookTableDom.scrollHeight ===
            Math.round(this.bookTableDom.scrollTop) + this.bookTableDom.clientHeight
        ) {

            this.props.onGetBooks(this.props.page + 1, this.props.limit);

        }

    };


    sortRows = (initialRows, sortColumn, sortDirection) => {
        const comparer = (a, b) => {
            if (sortDirection === "ASC") {
                return a[sortColumn] > b[sortColumn] ? 1 : -1;
            } else if (sortDirection === "DESC") {
                return a[sortColumn] < b[sortColumn] ? 1 : -1;
            }
        };
        if (sortDirection !== "NONE") {
            const books = this.props.books.slice();
            this.props.onUpdateListBooks(books.sort(comparer))
        }
    };
    openModal = () => {
        console.log(this)
        this.props.openModal({
            open: true,
            title: 'Alert Modal',
            message: "hello",
            closeModal: this.closeModal
        }, 'alert')
    }
    render() {
        const { DropDownEditor } = Editors;

        const Categories = [
            { id: "it", value: "IT" },
            { id: "cooking", value: "Cooking" },
            { id: "teen", value: "Teen" }
        ];
        const categoryTypeEditor = <DropDownEditor options={Categories} />;

        const columns = [
            { key: "id", name: "ID", sortable: true },
            { key: "bookName", name: "Book Name", editable: true, sortable: true },
            { key: "author", name: "Author", editable: true, sortable: true },
            {
                key: "description",
                name: "Description",
                editable: true,
                sortable: true
            },
            { key: "price", name: "Price", editable: true, sortable: true },
            {
                key: "category",
                name: "Category",
                editor: categoryTypeEditor,
                sortable: true
            }
        ];

        return (
            <div style={{ maxWidth: "90%", margin: "auto" }}>
                  <ModalContainer/>
                <div
                    className="tableButton"
                    style={{ float: "right", margin: " 0px 0 15px 0" }}
                >
                    <Button type="danger" style={{ marginRight: "5px" }} onClick={this.openModal}>
                        Add
                    </Button>                  
                    <SaveButton />
                    <DeleteButton />
                </div>

                <ReactDataGrid
                    columns={columns}
                    rowGetter={this.rowGetter}
                    rowsCount={this.props.books.length}
                    enableCellSelect={true}
                    onGridRowsUpdated={this.onGridRowsUpdated}
                    minHeight={500}
                    onGridSort={(sortColumn, sortDirection) =>
                        this.sortRows(this.props.books, sortColumn, sortDirection)
                    }
                    rowSelection={{
                        showCheckbox: true,
                        enableShiftSelect: true,
                        onRowsSelected: this.onRowsSelected,
                        onRowsDeselected: this.onRowsDeselected,
                        selectBy: {
                            indexes: this.props.selectedIndexes
                        }
                    }}
                />
                <div style={{ float: "right", marginBottom: "15px" }}>
                    Showing {this.props.books.length} of {this.props.count} item
                </div>
             
            </div>
        );
    }
}
const mapStateToProps = (state) => {

    return ({
        isLoading: state.BookReducer.isLoading,
        books: state.BookReducer.books,
        count: state.BookReducer.count,
        page: state.BookReducer.page,
        limit: state.BookReducer.limit,
        error: state.BookReducer.error,
        selectedIndexes: state.BookReducer.selectedIndexes,
        selectedIds: state.BookReducer.selectedIds,
        editedBooks: state.BookReducer.editedBooks
    })
};

const mapDispatchToProps = dispatch => {
    return {
        onGetBooks: (page, limit) => {
            dispatch(getBooks(page, limit));
        },
        onUpdateListBooks: (rows) => {
            dispatch(updateListBooks(rows))
        },
        onUpdateEditedBooks: (editedBooks) => {
            dispatch(updateEditedBooks(editedBooks))
        },
        onSelectRow: (selectedIndexes, selectedIds) => {
            dispatch(selectRow(selectedIndexes, selectedIds))
        },
        onDeselectRow: (selectedIndexes) => {
            dispatch(deselectRow(selectedIndexes))
        },
        openModal: (modalProps, modalType) => {
            dispatch(openModal(modalProps, modalType))
        }

    };
};
export default connect(mapStateToProps, mapDispatchToProps)(BookComponent);