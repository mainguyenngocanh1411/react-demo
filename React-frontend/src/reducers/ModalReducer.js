import { TYPES } from '../actions/ActionType';
const initialState = {
    modalProps: {
        open: false       
    },
    modalType: null

}

export default (state = initialState, action) => {
    switch (action.type) {
        case TYPES.OPEN_MODAL: {
            return {
                ...state,
                modalProps: action.modalProps,
                modalType: action.modalType,
            }
        }
        case TYPES.CLOSE_MODAL: {
            return initialState;
        }
        default:
            return state;

    }

}