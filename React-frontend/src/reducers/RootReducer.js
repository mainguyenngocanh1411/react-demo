import { combineReducers } from 'redux';
import BookReducer from './BookReducer'
import ModalReducer from './ModalReducer'

export default combineReducers({
    BookReducer,
    ModalReducer
  });