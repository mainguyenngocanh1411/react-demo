import { TYPES } from '../actions/ActionType';
const initialState = {
    isLoading: false,
    books: [],
    backupList: [],
    error: false,
    count: 0,
    page: 1,
    editedBooks: [],
    limit: 20,
    selectedIndexes: [],
    selectedIds: [],

}
export default (state = initialState, action) => {
    console.log(action)
    switch (action.type) {
        case TYPES.GET_BOOKS:
            return { ...state, isLoading: true, page: action.page, limit: action.limit };
        case TYPES.BOOKS_RECEIVED: {
            return { ...state, isLoading: false, books: [...state.books, ...action.data.books], backupList: [...state.backupList, ...action.data.books], count: action.data.count }
        }
        //SAVE
        case TYPES.SAVE_BOOKS:
            return { ...state, isLoading: true }
        case TYPES.SAVE_BOOKS_SUCCESS:
            return { ...state, backupList: state.books, editedBooks: [], isLoading: false }

        //UPDATE
        case TYPES.UPDATE_LIST_BOOKS:
            return { ...state, books: action.listUpdate }
        case TYPES.UPDATE_EDITED_BOOKS:
            return { ...state, editedBooks: action.editedBooks }
        //DELETE
        case TYPES.DELETE_BOOKS:
            return { ...state, isLoading: true }
        case TYPES.DELETE_BOOKS_SUCCESS:
            return { ...state, isLoading: false, selectedIds: [], selectedIndexes: [], error: false, backupList: state.books }

        //ADD
        case TYPES.ADD_BOOK:
            return { ...state, isLoading: true }
        case TYPES.ADD_BOOK_FAILED:
            return { ...state, error: action.error, isLoading: false }
        case TYPES.ADD_BOOK_SUCCESS:
            return { ...state, isLoading: false }

        //OTHERS
        case TYPES.SELECT_ROW:
            return { ...state, selectedIds: action.selectedIds, selectedIndexes: action.selectedIndexes }
        case TYPES.DESELECT_ROW:
            return { ...state, selectedIndexes: action.selectedIndexes }
        //ERROR HANDLER
        case TYPES.ACTION_FAILED: {
            return { ...state, isLoading: false, error: action.error, books: state.backupList }
        }

        default:
            return state;
    }
}