import { TYPES } from './ActionType';

export const openModal = (modalProps, modalType) => ({
    type: TYPES.OPEN_MODAL,
    modalProps,
    modalType
});

export const closeModal = () => ({
    type: TYPES.CLOSE_MODAL
});