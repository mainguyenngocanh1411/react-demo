import {TYPES}  from './ActionType';
export const getBooks = (page, limit) => ({
    type: TYPES.GET_BOOKS,
    page,
    limit
});

export const saveBooks = (listBooks) =>({
    type: TYPES.SAVE_BOOKS,
    listBooks
})

export const deleteBooks = (listIds, listUpdate) =>({
    type: TYPES.DELETE_BOOKS,
    listIds,
    listUpdate
})

export const updateListBooks = (newList) =>({
    type: TYPES.UPDATE_LIST_BOOKS,
    listUpdate: newList
})

export const updateEditedBooks = (editedBooks) => ({
    type: TYPES.UPDATE_EDITED_BOOKS,
    editedBooks
})

export const addBook = (book) =>({
    type: TYPES.ADD_BOOK,
    book
})

export const selectRow = (selectedIndexes, selectedIds) => ({
    type: TYPES.SELECT_ROW,
    selectedIds,
    selectedIndexes
})

export const deselectRow = (selectedIndexes)=>({
    type: TYPES.DESELECT_ROW,
    selectedIndexes
})