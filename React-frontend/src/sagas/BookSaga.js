import { put, call, takeLatest } from 'redux-saga/effects';
import { TYPES } from '../actions/ActionType';
import { api } from './api'


function* fetchBooks(action) {
    if (action.limit == undefined) action.limit = 20;
    if (action.page == undefined) action.page = 1;

    try {
        const response = yield call(api.getBooks, {
            limit: action.limit,
            page: action.page
        })
        yield put({ type: TYPES.BOOKS_RECEIVED, data: response.data });
    }
    catch (error) {
        yield put({ type: TYPES.ACTION_FAILED, error: error });
    }

}

function* saveBooks(action) {
    console.log(action)
    try {
        yield call(api.saveBooks, {
            "listBooks": action.listBooks
        });
        yield put({ type: TYPES.SAVE_BOOKS_SUCCESS })
   
        alert("Save successfully")
    }
    catch{
        yield put({ type: TYPES.ACTION_FAILED })
        alert("Save unsuccessfully")
    }

}

function* deleteBooks(action) {

    try {

        yield call(api.deleteBooks, {
            "listId": action.listIds
        });
        var listUpdate = action.listUpdate;
        yield put({ type: TYPES.UPDATE_LIST_BOOKS, listUpdate })
        yield put({ type: TYPES.DELETE_BOOKS_SUCCESS })


        alert("Delete successfully")
    }
    catch{
        yield put({ type: TYPES.ACTION_FAILED })
        alert("Delete unsuccessfully")
    }


}

//TODO
function* addBook(action) {

    try {

        yield call(api.addBooks,action.book);
        yield put({ type: TYPES.ADD_BOOK_SUCCESS })


        alert("Add successfully")
    }
    catch{
        yield put({ type: TYPES.ADD_BOOK_FAILED })
        alert("Add unsuccessfully")
    }

}

export const BookSaga = [
    takeLatest(TYPES.GET_BOOKS, fetchBooks),
    takeLatest(TYPES.SAVE_BOOKS, saveBooks),
    takeLatest(TYPES.DELETE_BOOKS, deleteBooks),
    takeLatest(TYPES.ADD_BOOK, addBook)
]