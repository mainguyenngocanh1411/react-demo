import axios from "axios";
import config from '../config/config'

export const api = {
    /**
     * Get list books with pagination
     * @param {*} pagination 
     */
    getBooks: (pagination) => {
        var url = config.baseUrl + 'books?page=' + pagination.page + '&limit=' + pagination.limit;
        return axios({
            method: "GET",
            url: url
        })

    },

    /**
     * Save list books
     * @param {*} listBooks 
     */
    saveBooks: (listBooks) => {
        var url = config.baseUrl + 'books/updateList';
        return axios({
            method: "POST",
            url: url,
            data: listBooks
        })
    },

    /**
     * Delete books
     */
    deleteBooks:(listId) =>{
        var url = config.baseUrl + 'books'
        return axios({
            method: "DELETE",
            url: url,
            data: listId
        })
    },

    /**
     * Add book
     */
    addBooks:(book) =>{
        var url = config.baseUrl + "books"
        return axios({
            method: "POST",
            url: url,
            data: book
        })
    }
}