import { all } from 'redux-saga/effects';
import { BookSaga } from './BookSaga'

export default function* rootSaga() {
    yield all([
        ...BookSaga
    ]);
}